# ROS in docker examples #
Here, have some examples of ROS with docker and networking and GUI and other stuff.

## Basics ##
Please refer to [ros docker wiki](https://wiki.ros.org/docker/Tutorials/Docker) and [ros docker images](https://github.com/osrf/docker_images) for your first steps.
Have a look [listener and talker](listener-and-talker) for a simple example.

## moveit tutorials ##
How setting up the moveit tutorials can be done in docker while maintaining full GUI capability can be seen in [moveit_tutorials](moveit_tutorials).

## opening webcam and saving stream to file ##
How to open a USB-camera and saving its video stream to disk can be seen in [webcam_to_video](webcam_to_video).
