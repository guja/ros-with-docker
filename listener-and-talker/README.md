# ROS in docker example
This is a small example of how running ROS in docker may be done.

## Basics
Please refer to https://wiki.ros.org/docker/Tutorials/Docker and https://github.com/osrf/docker_images for your first steps.

## CLI and build containers

Use `ros:${ROS_DISTRO}-ros-{core/base/robot/perception}` offering ROS functionality. 

## GUI
See https://wiki.ros.org/docker/Tutorials/GUI for your first steps.

### fast and dirty
Use `osrf/ros:${ROS_DISTRO}-desktop(-full)(-debian/ubuntu-version)` log in with your `uid:gid` and share your local x11 with the container.
```
docker run -it --rm \
    --user=$(id -u $USER):$(id -g $USER) \
    --env="DISPLAY" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    osrf/ros:melodic-desktop-full
```

Drawbacks:
 - your user is not named - you won't be able to change anything in the container
 - some applications need a home directory - since you have no name you won't have a home

### launch one container into existing network
Use `osrf/ros:${ROS_DISTRO}-desktop(-full)(-debian/ubuntu-version)` log in with your `uid:gid`.
Some shared volumes to be able to really use your local account in the container.

```
docker run -it --rm \
    --user=$(id -u $USER):$(id -g $USER) \
    --workdir="/home/$USER" \
    --volume="/home/$USER:/home/$USER" \
    --env="DISPLAY" \
    --volume="/etc/group:/etc/group:ro" \
    --volume="/etc/passwd:/etc/passwd:ro" \
    --volume="/etc/shadow:/etc/shadow:ro" \
    --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --network={NETWORK_WHERE_YOUR_OTHER_ROS_CONTAINER_LIVE} \
    -e ROS_MASTER_URI=http://ros-master:11311 \
    -e ROS_HOSTNAME=ros_visualizer \
    osrf/ros:melodic-desktop-full \
rqt
```

### docker-compose
See https://wiki.ros.org/docker/Tutorials/Compose for more details. If you want to add a GUI container you have to add to your `docker-compose.yml`:

```
services:
    viz:
        image: osrf/ros:${ROS_DISTRO}-desktop-full
        container_name: ros_visualizer
        depends_on:
            - ros-master
        networks:
            - ros
        environment:
            - "ROS_MASTER_URI=http://ros-master:11311"
            - "ROS_HOSTNAME=ros_visualizer"
            - "DISPLAY"
        #share your user to the container in order to access your x11
        user: 1000:1000 #set to your personal uid:gid (can be determined pasting 'echo "uid: $(id -u $USER) - gid: $(id -g $USER)" 'to your console
        volumes:
            - /tmp/.X11-unix:/tmp/.X11-unix:rw #share your x11 socket to the container - 
        command: rqt
```
Setting `user` and `ROS_DISTRO` via `.env` file is possible.
