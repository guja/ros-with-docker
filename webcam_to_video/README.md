# Save videos to disk #

Let's save some video to disk!

## containers ##
1. core - roscore
1. camera - read camera attached to `/dev/video0`
1. gui - rqt frontend
1. saver - actually saving the video

## magic ##
There is no magic - only dependencies.
This is why we build our own fancy docker image.

Opening and streaming camera is done with built-in functionality from package `usb_cam` with command `rosrun usb_cam usb_cam_node /dev/video0`.

Saving video is done with built-in functionality from package `image_view` with command `rosrun image_view video_recorder image:=/usb_cam/image_raw _filename:=/data/output.avi`.
